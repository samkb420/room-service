/**
 * room service
 */

const roomRepository = require('../repository/room.repository');

class roomService {
    constructor() { }

    async create(room) {
        return await roomRepository.create(room);
    }

    async findAll() {
        return await roomRepository.findAll();
    }

    async findOne(roomId) {
        return await roomRepository.findOne(roomId);
    }

    async update(roomId,room) {
        return await roomRepository.update(roomId,room);
    }

    async delete(roomId) {
        return await roomRepository.delete(roomId);
    }
}

module.exports = new roomService();