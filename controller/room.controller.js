/**
 * 
 * room controller
 */

const roomService = require('../service/room.service');

class roomController {
    constructor() { }

    async create(room) {
        return await roomService.create(room);
         
    }

    async findAll() {
        return await roomService.findAll();
        
    }

    async findOne(roomId) {
        return await roomService.findOne(roomId);
        
    }

    async update(roomId,room) {
        return await roomService.update(roomId,room);
       
    }

    async delete(roomId) {
        return await roomService.delete(roomId);
        
    }
}

module.exports = new roomController();

