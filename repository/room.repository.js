/**
 * room repository
 */
const { dbConfig } = require('../config/db.config');

const roomModel = require('../model/room.model');

class roomRepository {
    constructor() {
        dbConfig();
    
     }

    async create(room) {
        let data = {}
        try {
            data = await roomModel.create(room);
        } catch (error) {
            console.log(error);
        }

        return data;
    }

    async findAll() {
        const rooms = await roomModel.find({});
        return rooms
    }

    async findOne(roomId) {
        let data = {}
        try {
        data =await roomModel.findOne({ _id: roomId });
        } catch (error) {
            console.log(error);
        }
        return data;
    }

    async update(roomId, room) {
        let data = {}
        try {
        data = await roomModel.findByIdAndUpdate(roomId, room);
        } catch (error) {
            console.log(error);
        }
        return data;
    }

    async delete(roomId) {
        let data = {}
        try {
        data = await roomModel.findByIdAndDelete({_id: roomId});
        } catch (error) {
            console.log(error);
        }
        return data;
    }
}

module.exports = new roomRepository();

