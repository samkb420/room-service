/**
 * database configuration
 * mongodb connection
 */

const mongoose = require("mongoose");

// const dbUrl = "mongodb://mongo:27017/smartHome";
const dbUrl = "mongodb+srv://kenyaweb:123456789kw@cluster0.guqpke2.mongodb.net/smarthome?retryWrites=true&w=majority";

const dbConfig = () => {
  mongoose.connect(
    dbUrl,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err) => {
      if (!err) {
        console.log("MongoDB Connection Succeeded.");
      } else {
        console.log("Error in DB connection : " + err);
      }
    }
  );
};

module.exports = { dbConfig };
