/**
 * room model
 */

const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    image: {
        type: String,
        required: false
    },
    lights: {
        type: Boolean,
        required: false,
        default:false
    },
    doorstatus: {
        type: Boolean,
        required: false,
        default:true
    },

    temperature: {
        type: Number,
        required: false,
        default:24
    },
    devices: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Device'
    }],
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }],
 


});

const roomModel = mongoose.model('room', roomSchema);

module.exports = roomModel;

