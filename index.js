/**
 * entry point for the application
 */

 const express = require('express');
 const bodyParser = require('body-parser');
 require('dotenv').config()
 const cors = require('cors');
 
const roomController = require('./controller/room.controller');
 
 
 
 const app = express();
 const port =  process.env.PORT  || 3000 ;
 app.use(cors());
 app.use(bodyParser.json());
 

 
 app.get('/', (req, res) => {
        res.json({
            message: "Welcome to Smart Home",
            version: "1.0.0",
            author:"samuel Kago",
            service: "room service",
            path: "http://room/api/v1/"
        })
 });

app.get('/api/v1/rooms',(req,res)=>{
    roomController.findAll().then((data)=>{
        res.json(data);
    })
});

app.post('/api/v1/rooms',(req,res)=>{
    roomController.create(req.body).then((data)=>{
        res.json(data);
    })
}
);

app.put('/api/v1/rooms/:id',(req,res)=>{
    roomController.update(req.params.id,req.body).then((data)=>{
        res.json(data);
    })
}
);

app.delete('/api/v1/rooms/:id',(req,res)=>{
    roomController.delete(req.params.id).then((data)=>{
        res.json(data);
    })
}
);

app.get('/api/v1/rooms/:id',(req,res)=>{
    roomController.findOne(req.params.id).then((data)=>{
        res.json(data);
    })
}
);


 
 
 app.listen(port, () => {
     console.log(`Server listening on the port  ${port}`);
 })
